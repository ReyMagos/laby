<p style="text-align: center; font-style='Comicsans'; font-size: 16px"><b>Игра "Лабиринт"</b></p>

![](https://ie.wampi.ru/2022/02/09/cover.jpg)

<u>Идея проекта:</u> головоломка с элементами пятнашек, которая поможет развить логику и расчетливость.

<u>Правила игры:</u> перемещая блоки и пешку доберись до выхода. Обрати внимание, что нельзя ходить сквозь стены, перемещать блок, на котором стоишь, а так же перемещаться с коричневого уровня на зеленый (и наоборот) без лестницы. Зеленый уровень неустойчивый, поэтому на нем останавливаться также нельзя.

<u>Реализация:</u> игра сделана полностью на PYGAME

1. Все файлы в папке entity - это, как sprite в pygame - различные объекты. base - это файл с родительским классов IEntity для всех объектов.
2. gui - папка со всеми entity, которые являются графическим интерфейсом
3. utils - это папка с файлами, в которых лежат полезные дополнительные функции.
4. images - папка с картинками к игре. В ней также есть levels.json, содержащий информацию об уровнях игры, и walls.json, содержащий информацию о расположениии стен и лестниц для каждого отдельного блока.

event.py - работа с событиями

main.py - главный запускающий файл

config.json содержит информацию о размере игры и ее название

![](https://ie.wampi.ru/2022/02/09/130a37f5e55609f4e.jpg)

<u>Отдельное спасибо</u> иллюстратору обложки игры - Марии Белкановой
