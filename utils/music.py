import pygame


def music_player():
    pygame.mixer.music.load("images/music.mp3")
    pygame.mixer.music.play(-1)


def music_pause(f):
    if not f:
        pygame.mixer.music.pause()
    else:
        pygame.mixer.music.unpause()
