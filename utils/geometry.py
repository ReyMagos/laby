import pygame


def is_point_inside(point, box: pygame.Rect):
    return box.collidepoint(point[0], point[1])
