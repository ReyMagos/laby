class Direction:
    def __init__(self, k):
        self.k = k

    def __new(self, k):
        return type(self)(k)

    def left(self):
        return self.__new(0)

    def top(self):
        return self.__new(1)

    def right(self):
        return self.__new(2)

    def bottom(self):
        return self.__new(3)

    def opposite(self):
        return self.__new((self.k - 2) % 4)

    def __eq__(self, other):
        return isinstance(Direction, other) and other.k == self.k
