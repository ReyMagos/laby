import os
import pygame


def load_image(fullname, colorkey=None):
    if not os.path.isfile(fullname):
        print(f"File {fullname} not found")
        return
    image = pygame.image.load(fullname)
    if colorkey is not None:
        image = image.convert()
        if colorkey == -1:
            colorkey = image.get_at((0, 0))
        image.set_colorkey(colorkey)
    else:
        image = image.convert_alpha()
    return image


def rotate_image(image, deg):
    return pygame.transform.rotate(load_image(image), int(deg))


def glowing(surface):
    ...
