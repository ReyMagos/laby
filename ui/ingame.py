import pygame
from entity.base import IEntity
from ui.button import Button
from event import Events


class InGameMenu(IEntity):
    def __init__(self):
        super().__init__()

    def render(self, window):
        pygame.draw.rect(window, (145, 145, 101), (310, 0, 320, 350))
        # self.buttons.update()
        # self.buttons.draw(self.screen)


class WinText(IEntity):
    world_join_event = Events.create()

    def __init__(self):
        super().__init__()

        self.play_again_button = Button((205, 350), text="Играть снова", onclick=self.join_world, size=(230, 40),
                                  bg=(219, 174, 114), fg=(64, 57, 48),font=("Comicsans", 20))
        self.add(self.play_again_button)

    def join_world(self):
        self.remove(self.play_again_button)
        Events.post(self.world_join_event)


    def render(self, surface):
        pygame.draw.rect(surface, (102, 168, 76), (115, 180, 400, 250))
        pygame.draw.line(surface, (0, 0, 0), (115, 180), (515, 180), 2)
        pygame.draw.line(surface, (0, 0, 0), (115, 180), (115, 430), 2)
        pygame.draw.line(surface, (0, 0, 0), (515, 180), (515, 430), 2)
        pygame.draw.line(surface, (0, 0, 0), (115, 430), (515, 430), 2)

        pygame.font.init()
        font = pygame.font.SysFont("Comic Sans MS", 30)
        text = font.render("Ты выиграл!", False, (0, 0, 0))
        surface.blit(text, (220, 270))
        super().render(surface)


        # self.buttons2.update()
        # self.buttons2.draw(self.screen)
