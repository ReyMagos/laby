import pygame.font

from entity.base import IEntity
from utils.geometry import is_point_inside
from event import Events


class IClickHandler(IEntity):
    def __init__(self, pos, size, onclick=None):
        super().__init__()

        self.bounds = pygame.Rect(pos[0], pos[1], size[0], size[1])
        self.onclick = onclick
        self.pressed = False

        self.handlers = []

    def on_create(self, parent):
        self.handlers.append(Events.handler(pygame.MOUSEBUTTONDOWN, self.on_mouse_down))
        self.handlers.append(Events.handler(pygame.MOUSEBUTTONUP, self.on_mouse_up))

    def on_destroy(self, parent):
        for handler in self.handlers:
            Events.unbind_handler(handler)

    def on_mouse_down(self):
        if is_point_inside(pygame.mouse.get_pos(), self.bounds):
            self.pressed = True

    def on_mouse_up(self):
        self.pressed = False
        if self.onclick is not None and is_point_inside(pygame.mouse.get_pos(), self.bounds):
            try:
                self.onclick(self.f)
                if self.f:
                    self.f = 0
                else:
                    self.f = 1
            except Exception:
                self.onclick()


class Button(IClickHandler):
    def __init__(self, pos, size=None, text="", font=None, onclick=None, border=(0, 0, 0), bg=(255, 255, 255),
                 fg=(0, 0, 0), f=None):
        if font is None:
            self.font = pygame.font.SysFont("Comicsans", 38)
        else:
            self.font = pygame.font.SysFont(font[0],font[1])
        self.text = text

        self.border = border
        self.bg = bg
        self.fg = fg
        self.f = f
        self.text_size = self.font.size(self.text)

        if size is None:
            size = (self.text_size[0] + 10, self.text_size[1])

        super().__init__(pos, size, onclick)

    def render(self, surface):
        x, y, w, h = self.bounds

        pygame.draw.line(surface, self.border, (x, y), (x + w, y), 5)
        pygame.draw.line(surface, self.border, (x, y - 2), (x, y + h), 5)

        pygame.draw.line(surface, self.border, (x, y + h), (x + w, y + h), 5)
        pygame.draw.line(surface, self.border, (x + w, y + h), (x + w, y), 5)

        if type(self.bg[0]) == int:
            pygame.draw.rect(surface, self.bg, (x, y, w, h))
        else:
            if self.f:
                img = pygame.image.load(self.bg[1])
            else:
                img = pygame.image.load(self.bg[0])

            surface.blit(img, (x, y))

        surface.blit(self.font.render(self.text, True, self.fg),
                     (x + (w - self.text_size[0]) // 2, y + (h - self.text_size[1]) // 2))
