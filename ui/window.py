import pygame

from entity.base import IEntityContainer
from event import Events


class Window(IEntityContainer):
    def __init__(self, width, height, title="Window"):
        super().__init__()

        self.width = width
        self.height = height
        self.title = title

        self.screen = None

        self.running = False

    def open(self):
        pygame.init()

        self.screen = pygame.display.set_mode((self.width, self.height))
        pygame.display.set_caption(self.title)

        Events.handler(pygame.QUIT, self.close)

    def update(self):
        for entity in self.branches:
            entity.update()
        Events.process()

    def render(self):
        for entity in self.branches:
            entity.render(self.screen)
        pygame.display.flip()

    def close(self):
        self.running = False
        pygame.quit()

    def run(self):
        try:
            self.open()

            self.running = True
            while self.running:
                self.render()
                self.update()
        finally:
            self.close()
