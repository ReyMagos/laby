from entity.base import IEntity
from ui.button import Button
from utils.image import load_image
from event import Events
from utils.music import music_pause


class MainMenu(IEntity):
    world_join_event = Events.create()

    def __init__(self):
        super().__init__()

        self.play_button = None
        self.exit_button = None
        self.music_button = None

    def join_world(self):
        Events.post(self.world_join_event)

    def on_create(self, parent):
        self.play_button = Button((240, 390), text="Играть", onclick=self.join_world, size=(150, 50),
                                  bg=(219, 174, 114), fg=(64, 57, 48))
        self.exit_button = Button((240, 470), text="Выход", onclick=exit, size=(150, 50),
                                  bg=(219, 174, 114), fg=(64, 57, 48))

        self.music_button = Button((600, 0), onclick=music_pause, size=(30, 30),
                                   bg=("images/unmute.png", "images/mute.png"), fg=(64, 57, 48), f=0)

        self.add(self.play_button, self.exit_button, self.music_button)

    def on_destroy(self, parent):
        self.clear()

    def render(self, surface):
        image = load_image("images/cover.jpg")
        surface.blit(image, (0, 0))

        super().render(surface)
