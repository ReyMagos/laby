import pygame


class Events:
    __handlers = {}
    __hid = {}

    __event_id = pygame.USEREVENT
    __handler_id = 0

    @classmethod
    def handler(cls, event, function):
        if event not in cls.__handlers:
            cls.__handlers[event] = []
        cls.__handlers[event].append(cls.__handler_id)
        cls.__hid[cls.__handler_id] = function

        cls.__handler_id += 1
        return cls.__handler_id - 1

    @classmethod
    def unbind_handler(cls, handler):
        del cls.__hid[handler]

    @classmethod
    def process(cls):
        for event in pygame.event.get():
            if event.type in cls.__handlers:
                cls.__handlers[event.type] = [h for h in cls.__handlers[event.type] if h in cls.__hid]
                for handler in cls.__handlers[event.type]:
                    if cls.__hid[handler]():
                        break

    @classmethod
    def create(cls):
        cls.__event_id += 1
        return cls.__event_id - 1

    @classmethod
    def post(cls, event):
        pygame.event.post(pygame.event.Event(event))
