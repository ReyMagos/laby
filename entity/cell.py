import json

from ui.button import IClickHandler
from utils.image import *


class Cell(IClickHandler):
    def __init__(self, world, pos, cell_type, rotate):
        self.world = world
        self.pos = pos
        self.cell_type = cell_type
        self.rotate = rotate
        self.image = None
        self.focused = False
        self.walls = None
        self.bounds = world.get_cell_bounds(self.pos)

        super().__init__((self.bounds.x, self.bounds.y), self.bounds.size, onclick=self.on_click)

    def is_empty(self):
        return self.cell_type == 0

    def get_side(self, side):
        return self.walls[side.k]

    def on_click(self):
        self.world.cell_clicked(self)

    def on_create(self, parent):
        def rotate_list(lst, deg):
            return lst[deg // 90:] + lst[:deg // 90]

        super().on_create(parent)

        if not self.is_empty():
            self.image = rotate_image(f"images/{self.cell_type}.png", self.rotate)
            with open("images/walls.json") as file:  # TODO: better way to load this file once
                self.walls = rotate_list(json.load(file)[str(self.cell_type)], self.rotate)

    def on_destroy(self, parent):
        super().on_destroy(parent)
        self.image = None

    def render(self, surface):
        if not self.is_empty():
            surface.blit(self.image, (self.bounds.x, self.bounds.y))
            if self.focused:
                selection = pygame.Surface(self.bounds.size).convert_alpha()
                selection.fill((100, 100, 100, 128))
                surface.blit(selection, (self.bounds.x, self.bounds.y))
