import pygame

from ui.button import IClickHandler
from utils.direction import Direction


class Finish(IClickHandler):
    def __init__(self, world):
        self.world = world
        self.bounds = pygame.Rect(0, 45, 45, 90)

        super().__init__((0, 90), (45, 100), onclick=self.on_click)

    def on_click(self):
        if self.world.find_path(self.world.player_pos, 0) and \
                self.world.cells[0].get_side(Direction(0)) in {1, 3}:
            self.world.win = True
            print("WIN")
