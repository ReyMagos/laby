import pygame

from ui.button import IClickHandler
from utils.geometry import is_point_inside
from utils.image import load_image


class Player(IClickHandler):
    def __init__(self, world):
        self.world = world
        self.image = None
        self.bounds = world.get_player_bounds()
        self.focused = False

        super().__init__((self.bounds.x, self.bounds.y), self.bounds.size, onclick=self.on_click)

    def on_click(self):
        self.world.player_clicked()

    def on_mouse_up(self):
        self.pressed = False
        if self.onclick is not None and is_point_inside(pygame.mouse.get_pos(), self.bounds):
            self.onclick()
            return True

    def on_create(self, parent):
        super().on_create(parent)
        self.image = load_image("images/player.png")

    def on_destroy(self, parent):
        self.image = None

    def render(self, surface):
        surface.blit(self.image, (self.bounds.x, self.bounds.y))
        if self.focused:
            outline = pygame.mask.from_surface(self.image).outline()
            glowing = pygame.Surface(self.image.get_rect().size).convert_alpha()
            glowing.fill((0, 0, 0, 0))
            pygame.draw.polygon(glowing, (255, 155, 0, 150), outline, 0)
            surface.blit(glowing, (self.bounds.x, self.bounds.y))
