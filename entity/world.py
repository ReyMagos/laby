import json
import pygame
from random import randint
from collections import deque

from entity.base import IEntity
from entity.cell import Cell
from entity.finish import Finish
from entity.player import Player
from ui.ingame import WinText
from utils.direction import Direction
from utils.image import load_image
from event import Events


class World(IEntity):
    cell_moved_event = Events.create()

    def __init__(self):
        super().__init__()
        self.win = False
        self.cells = None
        self.focused_cell = None
        self.player_pos = None
        self.player = None

        self.move = []

        self.strange_thing = {0: (None, None, 1, 3), 1: (0, None, 2, 4), 2: (1, None, None, 5),
                              3: (None, 0, 4, 6), 4: (3, 1, 5, 7), 5: (4, 2, None, 8), 6: (None, 3, 7, None),
                              7: (6, 4, 8, None), 8: (7, 5, None, None)}

        self.finish = None

    def cell_clicked(self, cell):
        if self.player.focused:
            if self.can_player_stay_at(cell) and self.find_path(self.player_pos, cell.pos):
                self.player_pos = cell.pos
                self.player.bounds = self.get_player_bounds()
                self.player.focused = False
            return
        if self.focused_cell is None and not cell.is_empty():
            self.focused_cell = cell
            cell.focused = True
        elif self.focused_cell is not None:
            if self.focused_cell is not cell:
                self.move = [self.focused_cell, cell]
            else:
                self.focused_cell.focused = False
                self.focused_cell = None

    def is_path_between(self, u, v, side):
        if not self.cells[u].is_empty() and not self.cells[v].is_empty():
            return (self.cells[u].get_side(side), self.cells[v].get_side(side.opposite())) in {(0, 0), (1, 1), (1, 3),
                                                                                               (3, 1)}
        return False

    def find_path(self, cell, dest):
        used = {cell}
        q = deque()
        q.appendleft(cell)

        while len(q) > 0:
            u = q.pop()
            if u == dest:
                return True
            for i in range(4):
                v = self.strange_thing[u][i]
                if v is not None and v not in used and self.is_path_between(u, v, Direction(i)):
                    used.add(v)
                    q.appendleft(v)
        return False

    def can_player_stay_at(self, cell):
        return int(cell.cell_type) in {1, 2, 3, 7, 8}

    def player_clicked(self):
        if self.focused_cell is None:
            self.player.focused = not self.player.focused

    def get_cell_bounds(self, pos):
        return pygame.Rect(45 + 180 * (pos % 3), 45 + 180 * (pos // 3), 180, 180)

    def get_player_bounds(self):
        bounds = self.get_cell_bounds(self.player_pos)
        bounds.x += 60
        bounds.y += 60
        bounds.size = (60, 60)
        return bounds

    def move_cell(self, cell, dest):
        if dest.pos in self.strange_thing[cell.pos] and dest.is_empty() and cell.pos != self.player_pos:
            cell.pos, dest.pos = dest.pos, cell.pos
            cell.bounds, dest.bounds = dest.bounds, cell.bounds
            self.cells[cell.pos], self.cells[dest.pos] = self.cells[dest.pos], self.cells[cell.pos]
            self.focused_cell.focused = False
            self.focused_cell = None

    def on_create(self, window):
        self.load_level()

    def update(self):
        if self.move:
            self.move_cell(*self.move)
            self.move = []

    def render(self, surface):
        self.draw_game(surface)
        if self.win == 1:
            self.w = WinText()
            self.win = 2
        if self.win == 2:
            self.w.render(surface)
        else:
            self.w = None

    def draw_game(self, surface):
        surface.blit(load_image("images/background.png"), (0, 0))
        for cell in self.cells:
            cell.render(surface)
        self.player.render(surface)
        self.finish.render(surface)

    def load_level(self):
        with open("images/levels.json") as file:
            levels = json.load(file)

        level = levels[f"level_{randint(8, 16)}"]

        self.player_pos = level[-1]["player"]
        self.player = Player(self)
        self.player.on_create(self)

        self.cells = []

        for i in range(3):
            for j in range(3):
                k = i * 3 + j
                self.cells.append(Cell(self, k, level[k]["cell"], level[k]["rotate"]))
                self.cells[-1].on_create(self)

        self.finish = Finish(self)
        self.finish.on_create(self)
