class IEntityContainer:
    def __init__(self):
        self.branches = []

    def add(self, *entities):
        for entity in entities:
            self.branches.append(entity)
            entity.on_create(self)

    def remove(self, *entities):
        for entity in entities:
            self.branches.remove(entity)
            entity.on_destroy(self)

    def clear(self):
        for entity in self.branches:
            entity.on_destroy(self)
        self.branches.clear()


class IEntity(IEntityContainer):
    def on_create(self, parent):
        ...

    def on_destroy(self, parent):
        ...

    def render(self, surface):
        for entity in self.branches:
            entity.render(surface)

    def update(self):
        ...
