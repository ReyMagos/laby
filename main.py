from entity.world import World
from event import Events
from ui.menu import MainMenu
from ui.window import Window
from utils.music import music_player
from ui.ingame import WinText


class Labyrinth(Window):
    def __init__(self):
        super().__init__(630, 630)

        self.main_menu = None

    def open(self):
        super().open()

        music_player()
        self.main_menu = MainMenu()
        self.add(self.main_menu)

        Events.handler(MainMenu.world_join_event, self.join_world)
        Events.handler(WinText.world_join_event, self.join_world)

    def update(self):
        super().update()

    def render(self):
        super().render()

    def join_world(self):
        self.clear()
        self.add(World())


def main():
    labyrinth = Labyrinth()
    labyrinth.run()


if __name__ == "__main__":
    main()
